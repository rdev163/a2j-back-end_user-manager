package handlers

import (
	"errors"
	"github.com/gammazero/nexus/wamp"
	"log"
	"os"
)

const (
	checkUsernameProcName = "store.endusers.check.username"
    checkEmailProcName = "store.endusers.check.email"
    createUserProcName = "store.endusers.create"
)

type ProcedureMessage struct {
	Headers  map[string]interface{}
	MessageType string
	Payload map[string]interface{}
	Options map[string]interface{}
}

type UserData struct {
	username string
	email string
	role int
	password string
}

var logger = log.New(os.Stdout, "Handlers > ", log.LstdFlags)

/**
 * Builds a ProcedureMessage from a wamp.dict from a procedure call and checks for a header.
 */
func handleProcedureMessage(dicts wamp.Dict) (ProcedureMessage, error) {

	headers, ok := dicts["Headers"].(map[string]interface{})
	if !ok {
		return ProcedureMessage{}, errors.New("no header") // header required
	}

	payload, ok := dicts["Payload"].(map[string]interface{})
	if !ok {
		payload = map[string]interface{}{}
	}

	messageType, ok := dicts["Type"].(string)
	if !ok {
		messageType = ""
	}

	options, ok := dicts["Options"].(map[string]interface{})
	if !ok {
		options = map[string]interface{}{}
	}

	return ProcedureMessage{
		Headers: headers,
		MessageType: messageType,
		Payload: payload,
		Options: options,
	}, nil
}
