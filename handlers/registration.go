package handlers

import (
	"context"
	"crypto/rand"
	"errors"
	"time"

	client2 "github.com/CodeForPortland/ctl-aura/aura/client"

	"github.com/CodeForPortland/a2j-back-end_data-store-manager/models"

	"github.com/CodeForPortland/a2j-back-end_user-manager/config"
	"github.com/CodeForPortland/a2j-back-end_user-manager/utils"

	"github.com/dgrijalva/jwt-go"

	"golang.org/x/crypto/argon2"

	"github.com/gammazero/nexus/client"
	"github.com/gammazero/nexus/wamp"
)

/**
 * Extracts user data from a procedure message and checks to ensure needed fields have values provided.
 */
func newUserDataMessage(message ProcedureMessage) (UserData, error) {
	role := 1

	username, ok := message.Payload["username"].(string)
	if !ok {
		return UserData{}, errors.New("failed to provide valid user data")
	}

	email, ok := message.Payload["email"].(string)
	if !ok {
		return UserData{}, errors.New("failed to provide valid user data")
	}

	password, ok := message.Payload["password"].(string)
	if !ok {
		return UserData{}, errors.New("failed to provide valid user data")
	}

	return UserData{username:username, email: email, password: password, role: role}, nil

}

/**
 * Manages the process flow for triggering data validations and an end user record creation. It responds with an error or a JWT as a session token
 */
var Registration client.InvocationHandler = func(context context.Context, lists wamp.List, dicts wamp.Dict, dicts2 wamp.Dict) *client.InvokeResult {

	localConfigs := &config.Configs{}
	localConfigs.Init()

	internalCLI := client2.AuraClient{Realm: localConfigs.InternalRealm.Realm, URL: localConfigs.InternalRealm.URL, Logger: logger}
	internalCLI.New(client2.ConnectWS)

	message, err := handleProcedureMessage(dicts)
	userData, err := newUserDataMessage(message)
	if err != nil {
		logger.Println("procedure message failed: ", err)
		return &client.InvokeResult{Err: wamp.ErrCanceled}
	}

	//todo Check Validate Nonce

// /todo refactor
	// Check Username is available
	result, err := internalCLI.Client.Call(context, checkUsernameProcName, nil, nil, wamp.Dict{"Payload": userData.username}, "")
	if err != nil {
		logger.Println("Error calling procedure: ", checkUsernameProcName)
		return &client.InvokeResult{Err: wamp.ErrCanceled}
	}
	response := result.ArgumentsKw["Payload"].(bool)
	if response {
		logger.Println("username not available")
		return &client.InvokeResult{Err: wamp.ErrCanceled}
	}

	// check email is valid
	if !utils.IsValidEmail(userData.email) {
		logger.Println("email is invalid")
		return &client.InvokeResult{Err: wamp.ErrCanceled}
	}

	// Check if Email is unique
	result, err = internalCLI.Client.Call(context, checkEmailProcName, nil,  nil, wamp.Dict{"Payload": userData.email}, "")
	if err != nil {
		logger.Println("Error calling procedure: ", checkUsernameProcName)
		return &client.InvokeResult{Err: wamp.ErrCanceled}
	}
	response = result.ArgumentsKw["Payload"].(bool)
	if response {
		logger.Println("email not available")
		return &client.InvokeResult{Err: wamp.ErrCanceled}
	}

	// check if password is valid
	if !utils.IsPasswordValid(userData.password) {
		logger.Println("password not valid")
		return &client.InvokeResult{Err: wamp.ErrCanceled}
	}

	  //*===========================================================================//
	 //  Todo Extract to security module/service with local config for conventions //
	//===========================================================================*//

	// hash password
	salt := localConfigs.Security.Salt

	// string to byte array
	passBytes := []byte(userData.password)
	// salt to byte array
	saltBytes := []byte(userData.username + userData.email + salt)

	hash := argon2.IDKey(passBytes, saltBytes, 1, 64*1024, 4, 32)

	logger.Println("pw hashed", hash) //todo Remove

	opaqueSessionToken := make([]byte, 128)

	key := make([]byte, 128)

	_, err = rand.Read(opaqueSessionToken)
	_, err = rand.Read(key)
	if err != nil {
		logger.Panicln("error with security token generation")
		return &client.InvokeResult{Err: wamp.ErrCanceled}
	}
	  //*============================================================================//
	 // </Todo Extract to security module/service with local config for conventions //
	//============================================================================*//

	endUser := models.EndUser{Username: userData.username, PrimaryEmail: userData.email, HashedPass: hash, Role: models.Applicant }

	// Store EndUser Obj

	result, err = internalCLI.Client.Call(context, createUserProcName, nil, nil,  wamp.Dict{"Payload": endUser}, "")
	if err != nil {
		logger.Println("error with store procedure")
		return &client.InvokeResult{Err: wamp.ErrCanceled}
	}

	logger.Println("Result from store procedure", result)

	createModel := result.ArgumentsKw["Payload"].(map[string]interface{})

	// Make Session Token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sessionToken": opaqueSessionToken,
		"userData": createModel,
		"signedIn": true,
		"nbf": time.Now().Local().Add(time.Hour * 6).Unix(),
		//"nbf": time.Date(2015, 10, 10, 12, 0, 0, 0, time.UTC).Unix(),
	})

	// Sign and get the complete encoded token as a string using the secret
	tokenString, err := token.SignedString(key)

	//todo send verification email

	//smtp.Client{}

	// Return Session Token
	return &client.InvokeResult{
		Kwargs: wamp.Dict{
			"Headers": message.Headers,
			"Type": "Response",
			"Payload": map[string] string {
				"sessionToken": tokenString,
			}}}
}
