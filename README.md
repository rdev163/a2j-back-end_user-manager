# a2j-back-end_user-manager

[![Waffle.io - Columns and their card count](https://badge.waffle.io/CodeForPortland/a2j-back-end_user-manager.svg?columns=all)](https://waffle.io/CodeForPortland/a2j-back-end_user-manager)

A orchestrator of processes need to handle end user communication and data management.  
