package procedures

import (
	"github.com/CodeForPortland/a2j-back-end_user-manager/config"
	"github.com/CodeForPortland/a2j-back-end_user-manager/handlers"
	"github.com/CodeForPortland/ctl-aura/aura/client"
	"log"
	"os"
)

func Initialize() error {
	logger := log.New(os.Stdout, "Procedures > ", log.LstdFlags)

	// get configs
	localConfigs := &config.Configs{}
	localConfigs.Init()
	//if err != nil {
	//	logger.Panicln("local configs are not being properly set: ", err)
	//	return err
	//}
	logger.Println("configs", localConfigs)
	cli := client.AuraClient{Realm: localConfigs.PublicRealm.Realm , URL:"localhost:3131", Logger: logger}
	cli.New(client.ConnectWS)

	// set procedures
	err := cli.Client.Register("user.registration", handlers.Registration, nil)
	if err != nil {
		logger.Panicln("failed to register user.registration: ", err)
		return err
	}
	logger.Println("Procedures initialized.")
	return nil
}