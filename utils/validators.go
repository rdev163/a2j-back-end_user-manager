package utils

import "regexp"

func IsValidEmail(email string) bool {
	re := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	return re.MatchString(email)
}

type Constraint struct {
	Type string
	MustMatch bool
	Pattern string
}

func IsPasswordValid(password string, constraints ...Constraint) bool {
	//todo more robust validation

	if len(password) > 8 {
		return true
	}
	return false

	//for _, constraint := range constraints {
	//	if constraint.Type == "Length" {
	//      ...
	//	}
	//}
}