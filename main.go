package main

import (
	"github.com/CodeForPortland/a2j-back-end_user-manager/procedures"
	"log"
	"os"
	"os/signal"
)

func main() {
	logger := log.New(os.Stdout, "User Manager > ", log.LstdFlags)

	procedures.Initialize()

	sigChan := make(chan os.Signal, 1)

	signal.Notify(sigChan, os.Interrupt)

	select {

	case <-sigChan:

		logger.Println("Shutting Down!")

	}
}