package config

import (
	"github.com/spf13/viper"
	"log"
	"os"
)

type Realm struct {
	Label string
	Realm string
	URL   string
}

type Security struct {
	Salt string
}

type Configs struct {
	InternalRealm Realm
	PublicRealm Realm
	Security Security
}

var defaultConfig = Configs{
	InternalRealm: Realm{"internal", "ctl.aura.internal", "localhost:3131"},
	PublicRealm: Realm{"public", "ctl.aura.public", "localhost:3131"},
	Security: Security{
		Salt: "A2J-Default-!@AWEFsdfgw!#@Q#%Y$SG34twafGQ@$%TWAGA",
	},
}

func (c *Configs) Init() {
	logger := log.New(os.Stdout, "Config > ", log.LstdFlags)
	cwd, err := os.Getwd()
	configPath := cwd + "/local"

	viper.SetConfigType("json")
	viper.SetConfigName("local.config")
	viper.AddConfigPath(configPath)
	err = viper.ReadInConfig()
	if err != nil {
		// default fallback
		c = &defaultConfig
		logger.Println("No local/local.config.json file found, using default configs.", err)
		return
	}

	// internal
	realm := viper.Get("realms.internal.realm").(string)
	url := viper.Get("realms.internal.url").(string)
	c.InternalRealm = Realm{Label: "internal", Realm: realm, URL: url}

	// public
	realm = viper.Get("realms.public.realm").(string)
	url = viper.Get("realms.public.url").(string)
	c.PublicRealm = Realm{Label: "public", Realm: realm, URL: url}

	// security
	salt := viper.Get("security.salt").(string)
	c.Security.Salt = salt
}